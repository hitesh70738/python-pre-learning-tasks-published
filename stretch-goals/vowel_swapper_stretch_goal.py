def vowel_swapper(string):
    # ==============
    # Your code here
    counts_dict = {"a": 0, "e": 0, "i": 0, "o": 0, "u": 0}
    new_string = ""
   
    for c in string:
        if c == "a" or c == "A":
            counts_dict["a"] += 1
            if counts_dict["a"] == 2:
                new_string += "4"
            else:
                new_string += c
        elif c == "e" or c == "E":
            counts_dict["e"] += 1
            if counts_dict["e"] == 2:
                new_string += "3"
            else:
                new_string += c
        elif c == "i" or c == "I":
            counts_dict["i"] += 1
            if counts_dict["i"] == 2:
                new_string += "!"
            else:
                new_string += c
        elif c == "o" or c == "O":
            counts_dict["o"] += 1
            if counts_dict["o"] == 2:
                if c == "o":
                    new_string += "ooo"
                else:
                    new_string += "OOO"
            else:
                new_string += c
        elif c == "u" or c == "U":
            counts_dict["u"] += 1
            if counts_dict["u"] == 2:
                new_string += "|_|"
            else:
                new_string += c
        elif c == " ":
            new_string += " "
        else:
            new_string += c
       
    return new_string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
