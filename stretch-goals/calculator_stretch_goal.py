def calculator(a, b, operator):
    # ==============
    # Your code here
      to_bin = lambda x: "{0:b}".format(x)
    
      if operator == '+':
        return to_bin(a+b)
      elif operator == '-':
        return to_bin(a-b)
      elif operator == '*':
        return to_bin(a*b)
      elif operator == '/':
        return to_bin(a//b)
      else:
        print('redo')
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
